import request from '@/utils/request'

// 查询物种信息列表
export function listSpecyInfo(query) {
    return request({
        url: '/system/specyInfo/list',
        method: 'get',
        params: query
    })
}

// 查询物种信息详细
export function getSpecyInfo(specyId) {
    return request({
        url: '/system/specyInfo/' + specyId,
        method: 'get'
    })
}

// 新增物种信息
export function addSpecyInfo(data) {
    return request({
        url: '/system/specyInfo',
        method: 'post',
        data: data
    })
}

// 修改物种信息
export function updateSpecyInfo(data) {
    return request({
        url: '/system/specyInfo',
        method: 'put',
        data: data
    })
}

// 删除物种信息
export function delSpecyInfo(specyId) {
    return request({
        url: '/system/specyInfo/' + specyId,
        method: 'delete'
    })
}

// 导出物种信息
export function exportSpecyInfo(query) {
    return request({
        url: '/system/specyInfo/export',
        method: 'get',
        params: query
    })
}