import request from '@/utils/request'

// 查询植物详细信息列表
export function listPlantInfo(query) {
    return request({
        url: '/system/specyInfo/list',
        method: 'get',
        params: query
    })
}

// 查询植物详细信息详细
export function getPlantInfo(specyId) {
    return request({
        url: '/system/plantInfo/' + specyId,
        method: 'get'
    })
}

// 新增植物详细信息
export function addPlantInfo(data) {
    return request({
        url: '/system/plantInfo',
        method: 'post',
        data: data
    })
}

// 修改植物详细信息
export function updatePlantInfo(data) {
    return request({
        url: '/system/plantInfo',
        method: 'put',
        data: data
    })
}

// 删除植物详细信息
export function delPlantInfo(specyId) {
    return request({
        url: '/system/plantInfo/' + specyId,
        method: 'delete'
    })
}

// 导出植物详细信息
export function exportPlantInfo(query) {
    return request({
        url: '/system/plantInfo/export',
        method: 'get',
        params: query
    })
}