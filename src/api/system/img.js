import request from "@/utils/request";

// 图像识别ByImage
export function imageRecognitionByImage(image) {
  let formdata = new FormData()
  formdata.append("image",image)
  return request({
    url: "/system/imageRecognition",
    method: "post",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    data:formdata
  });
}

// 图像识别ByUrl
export function imageRecognitionByUrl(imgUrl) {
  return request({
    url: "/system/imageRecognition?imgUrl=" + imgUrl,
    method: "post"
  });
}
