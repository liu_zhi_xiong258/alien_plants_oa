import request from '@/utils/request'

// 查询app版本信息列表
export function listVersionInfo(query) {
    return request({
        url: '/system/versionInfo/list',
        method: 'get',
        params: query
    })
}

// 查询app版本信息详细
export function getVersionInfo(versionId) {
    return request({
        url: '/system/versionInfo/' + versionId,
        method: 'get'
    })
}

// 新增app版本信息
export function addVersionInfo(data) {
    return request({
        url: '/system/versionInfo',
        method: 'post',
        data: data
    })
}

// 修改app版本信息
export function updateVersionInfo(data) {
    return request({
        url: '/system/versionInfo',
        method: 'put',
        data: data
    })
}

// 删除app版本信息
export function delVersionInfo(versionId) {
    return request({
        url: '/system/versionInfo/' + versionId,
        method: 'delete'
    })
}

// 导出app版本信息
export function exportVersionInfo(query) {
    return request({
        url: '/system/versionInfo/export',
        method: 'get',
        params: query
    })
}


// // 导出app版本信息
// export function downloadFile(versionId) {
//     return request({
//         url: `/common/download/apk/${versionId}`,
//         method: 'get'
//     })
// }