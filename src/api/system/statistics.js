import request from '@/utils/request'

// 获取地区物种近5年发生面积
export function getSpecyArea(query) {
    return request({
        url: '/system/statistics/specyArea',
        method: 'get',
        params: query
    })
}

// 获取地区下级行政区某物种发生面积
export function getSpecyAreaByRegion(query) {
    return request({
        url: '/system/statistics/specyAreaByRegion',
        method: 'get',
        params: query
    })
}

// 获取该地区下级行政区的上传物种数
export function getSpecyNum(query) {
    return request({
        url: '/system/statistics/specyNum',
        method: 'get',
        params: query
    })
}

// 获取调查信息中的物种名
export function getSpecyName() {
    return request({
        url: '/system/statistics/specyName',
        method: 'get'
    })
}