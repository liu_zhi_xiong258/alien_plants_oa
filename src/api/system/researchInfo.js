import request from '@/utils/request'

// 查询调查信息列表
export function listResearchInfo(query) {
    return request({
        url: '/system/researchInfo/list',
        method: 'get',
        params: query
    })
}

// 查询调查信息详细
export function getResearchInfo(researchId) {
    return request({
        url: '/system/researchInfo/' + researchId,
        method: 'get'
    })
}

// 新增调查信息
export function addResearchInfo(data) {
    return request({
        url: '/system/researchInfo',
        method: 'post',
        data: data
    })
}

// 修改调查信息
export function updateResearchInfo(data) {
    return request({
        url: '/system/researchInfo',
        method: 'put',
        data: data
    })
}

// 删除调查信息
export function delResearchInfo(researchId) {
    return request({
        url: '/system/researchInfo/' + researchId,
        method: 'delete'
    })
}

// 导出调查信息
export function exportResearchInfo(query) {
    return request({
        url: '/system/researchInfo/export',
        method: 'get',
        params: query
    })
}