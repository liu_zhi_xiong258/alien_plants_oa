import request from '@/utils/request'

// 查询微生物列表
export function listMicrobeInfo(query) {
    return request({
        url: '/system/specyInfo/list',
        method: 'get',
        params: query
    })
}

// 查询微生物详细
export function getMicrobeInfo(specyId) {
    return request({
        url: '/system/microbeInfo/' + specyId,
        method: 'get'
    })
}

// 新增微生物
export function addMicrobeInfo(data) {
    return request({
        url: '/system/microbeInfo',
        method: 'post',
        data: data
    })
}

// 修改微生物
export function updateMicrobeInfo(data) {
    return request({
        url: '/system/microbeInfo',
        method: 'put',
        data: data
    })
}

// 删除微生物
export function delMicrobeInfo(specyId) {
    return request({
        url: '/system/microbeInfo/' + specyId,
        method: 'delete'
    })
}

// 导出微生物
export function exportMicrobeInfo(query) {
    return request({
        url: '/system/microbeInfo/export',
        method: 'get',
        params: query
    })
}