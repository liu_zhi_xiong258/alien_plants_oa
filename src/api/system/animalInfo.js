import request from '@/utils/request'

// 查询动物详细信息列表
export function listAnimalInfo(query) {
    return request({
        url: '/system/specyInfo/list',
        method: 'get',
        params: query
    })
}

// 查询动物详细信息详细
export function getAnimalInfo(specyId) {
    return request({
        url: '/system/animalInfo/'+specyId,
        method: 'get'
    })
}

// 新增动物详细信息
export function addAnimalInfo(data) {
    return request({
        url: '/system/animalInfo',
        method: 'post',
        data: data
    })
}

// 修改动物详细信息
export function updateAnimalInfo(data) {
    return request({
        url: '/system/animalInfo',
        method: 'put',
        data: data
    })
}

// 删除动物详细信息
export function delAnimalInfo(specyId) {
    return request({
        url: '/system/animalInfo/' + specyId,
        method: 'delete'
    })
}

// 导出动物详细信息
export function exportAnimalInfo(query) {
    return request({
        url: '/system/animalInfo/export',
        method: 'get',
        params: query
    })
}