import request from '@/utils/request'

// 查询分析信息列表
export function listAnalysisInfo(query) {
    return request({
        url: '/system/analysisInfo/list',
        method: 'get',
        params: query
    })
}

// 查询分析信息详细
export function getAnalysisInfo(analysisId) {
    return request({
        url: '/system/analysisInfo/' + analysisId,
        method: 'get'
    })
}

// 新增分析信息
export function addAnalysisInfo(data) {
    return request({
        url: '/system/analysisInfo',
        method: 'post',
        data: data
    })
}

// 修改分析信息
export function updateAnalysisInfo(data) {
    return request({
        url: '/system/analysisInfo',
        method: 'put',
        data: data
    })
}

// 删除分析信息
export function delAnalysisInfo(analysisId) {
    return request({
        url: '/system/analysisInfo/' + analysisId,
        method: 'delete'
    })
}

// 导出分析信息
export function exportAnalysisInfo(query) {
    return request({
        url: '/system/analysisInfo/export',
        method: 'get',
        params: query
    })
}