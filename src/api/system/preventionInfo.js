import request from '@/utils/request'

// 查询物种防控信息列表
export function listPreventionInfo(query) {
    return request({
        url: '/system/preventionInfo/list',
        method: 'get',
        params: query
    })
}

// 查询物种防控信息详细
export function getPreventionInfo(preventionId) {
    return request({
        url: '/system/preventionInfo/' + preventionId,
        method: 'get'
    })
}

// 查询物种防控信息详细
export function getPreventionSpecy() {
    return request({
        url: '/system/preventionInfo/getSpecy',
        method: 'get'  
    })
}

// 新增物种防控信息
export function addPreventionInfo(data) {
    return request({
        url: '/system/preventionInfo',
        method: 'post',
        data: data
    })
}

// 修改物种防控信息
export function updatePreventionInfo(data) {
    return request({
        url: '/system/preventionInfo',
        method: 'put',
        data: data
    })
}

// 删除物种防控信息
export function delPreventionInfo(preventionId) {
    return request({
        url: '/system/preventionInfo/' + preventionId,
        method: 'delete'
    })
}

// 导出物种防控信息
export function exportPreventionInfo(query) {
    return request({
        url: '/system/preventionInfo/export',
        method: 'get',
        params: query
    })
}