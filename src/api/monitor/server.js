import request from '@/utils/request'

// 查询服务器详细
export function getServer() {
  return request({
    url: '/monitor/server',
    method: 'get'
  })
}
//优雅关闭
export function getElegant() {
  return request({
    url: '/monitor/shutdown',
    method: 'post'
  })
}
