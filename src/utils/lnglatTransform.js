import Axios from "axios";

export function toDegrees(val) {
  if (typeof val == "undefined" || val == "") {
    return "";
  }
  var i = val.indexOf(".");
  var strDu = i < 0 ? val : val.substring(0, i); //获取度
  var strFen = 0;
  var strMiao = 0;
  if (i > 0) {
    var strFen = "0" + val.substring(i);
    strFen = strFen * 60 + "";
    i = strFen.indexOf(".");
    if (i > 0) {
      strMiao = "0" + strFen.substring(i);
      strFen = strFen.substring(0, i); //获取分
      strMiao = strMiao * 60 + "";
      i = strMiao.indexOf(".");
      strMiao = strMiao.substring(0, i + 4); //取到小数点后面三位
      strMiao = parseFloat(strMiao).toFixed(2); //精确小数点后面两位
    }
  }
  return strDu + "°" + strFen + "′" + strMiao + "″";
}

export async function getlnglatTransform(place) {
  let result = "";
  await Axios.get(
    `https://restapi.amap.com/v3/geocode/geo?address=${place}&key=00ede8aa42202422962024e94ebafa8e`
  ).then(res => {
    let lnglat = res.data.geocodes[0].location.split(",");
    result =
      "经度：" +
      toDegrees(lnglat[0].toString()) +
      "，" +
      "纬度：" +
      toDegrees(lnglat[1].toString());
  });
  return result
} 