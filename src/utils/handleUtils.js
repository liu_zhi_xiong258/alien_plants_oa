export function clearItemUtil(index, place, flagStr) {
  // 对分析和调查信息进行item的清空
  var obj = {};
  if (flagStr == "researchPlace") {
    obj.researchPlace = place;
    switch (index) {
      case 0:
        obj.optionCity = [];
        obj.valueCity = "";
        obj.optionCounty = [];
        obj.valueCounty = "";
        obj.optionTown = [];
        obj.valueTown = "";
        obj.researchPlace[1] = "";
        obj.researchPlace[2] = "";
        obj.researchPlace[3] = "";
        break;
      case 1:
        obj.optionCounty = [];
        obj.valueCounty = "";
        obj.optionTown = [];
        obj.valueTown = "";
        obj.researchPlace[2] = "";
        obj.researchPlace[3] = "";
        break;
      case 2:
        obj.optionTown = [];
        obj.valueTown = "";
        obj.researchPlace[3] = "";
        break;
      default:
        obj.optionCity = [];
        obj.valueCity = "";
        obj.optionCounty = [];
        obj.valueCounty = "";
        obj.optionTown = [];
        obj.valueTown = "";
        obj.researchPlace[1] = "";
        obj.researchPlace[2] = "";
        obj.researchPlace[3] = "";
    }
  } else if (flagStr == "analysisPlace") {
    obj.analysisPlace = place;
    switch (index) {
      case 0:
        obj.optionCity = [];
        obj.valueCity = "";
        obj.optionCounty = [];
        obj.valueCounty = "";
        obj.optionTown = [];
        obj.valueTown = "";
        obj.analysisPlace[1] = "";
        obj.analysisPlace[2] = "";
        obj.analysisPlace[3] = "";
        break;
      case 1:
        obj.optionCounty = [];
        obj.valueCounty = "";
        obj.optionTown = [];
        obj.valueTown = "";
        obj.analysisPlace[2] = "";
        obj.analysisPlace[3] = "";
        break;
      case 2:
        obj.optionTown = [];
        obj.valueTown = "";
        obj.analysisPlace[3] = "";
        break;
      default:
        obj.optionCity = [];
        obj.valueCity = "";
        obj.optionCounty = [];
        obj.valueCounty = "";
        obj.optionTown = [];
        obj.valueTown = "";
        obj.analysisPlace[1] = "";
        obj.analysisPlace[2] = "";
        obj.analysisPlace[3] = "";
    }
  }
  return obj;
}
